//
//  BZKTextField.h
//  IBDesignable
//
//  Created by Ringo on 7/7/2559 BE.
//  Copyright © 2559 Fungjai. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BZKTextField : UITextField

@property (nonatomic,setter=setPlaceHolderColor:) IBInspectable UIColor *placeHolderColor;
@property (nonatomic,setter=setUnderlineColor:) IBInspectable UIColor *underlineColor;

@end
