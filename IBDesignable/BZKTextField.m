//
//  BZKTextField.m
//  IBDesignable
//
//  Created by Ringo on 7/7/2559 BE.
//  Copyright © 2559 Fungjai. All rights reserved.
//

#import "BZKTextField.h"

@interface BZKTextField ()

@property (nonatomic,strong) UIView *underlineView;

@end

@implementation BZKTextField

- (void)prepareForInterfaceBuilder{
    [super prepareForInterfaceBuilder];
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
//    [self setup];
}

- (void)setup{
    [self setupUnderline];
}

- (void)setupUnderline{
    self.underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
    [self.underlineView setBackgroundColor:self.underlineColor];
    [self addSubview:self.underlineView];
    self.underlineView.hidden = self.underlineColor == nil;
}

- (void)setPlaceHolderColor:(UIColor*)color{
    _placeHolderColor = color;
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: color}];
}

- (void)setUnderlineColor:(UIColor *)underlineColor{
    _underlineColor = underlineColor;
    self.underlineView.backgroundColor = underlineColor;
    self.underlineView.hidden = underlineColor == nil;
}

@end
