//
//  ViewController.m
//  IBDesignable
//
//  Created by Ringo on 7/7/2559 BE.
//  Copyright © 2559 Fungjai. All rights reserved.
//

#import "ViewController.h"
#import "BZKTextField.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    BZKTextField *textField = [[BZKTextField alloc] initWithFrame:CGRectMake(10, 40, 200, 30)];
    textField.placeholder = @"Place holder";
    textField.placeHolderColor = [UIColor blueColor];
    textField.underlineColor = [UIColor darkGrayColor];
//    [textField performSelector:@selector(setup)];
    [self.view addSubview:textField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
