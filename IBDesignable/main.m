//
//  main.m
//  IBDesignable
//
//  Created by Ringo on 7/7/2559 BE.
//  Copyright © 2559 Fungjai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
